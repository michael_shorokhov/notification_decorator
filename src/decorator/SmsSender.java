package decorator;

import dto.NotificationSenderDTO;
import service.NotificationSenderDecorator;
import util.NotificationSender;

public class SmsSender extends NotificationSenderDecorator {
	public SmsSender(NotificationSender notificationSender) {
		super(notificationSender);
	}

	public void send(NotificationSenderDTO notificationSenderDTO){
		super.send(notificationSenderDTO);
		System.out.println(notificationSenderDTO.getSmsContent());
	}
}
