package decorator;

import dto.NotificationSenderDTO;
import service.NotificationSenderDecorator;
import util.NotificationSender;

public class EmailSender extends NotificationSenderDecorator {

	public EmailSender(NotificationSender notificationSender) {
		super(notificationSender);
	}

	public void send(NotificationSenderDTO notificationSenderDTO){
		super.send(notificationSenderDTO);
		System.out.println(notificationSenderDTO.getEmailContent());
	}
}
