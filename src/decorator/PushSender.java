package decorator;

import dto.NotificationSenderDTO;
import service.NotificationSenderDecorator;
import util.NotificationSender;

public class PushSender extends NotificationSenderDecorator {
	public PushSender(NotificationSender notificationSender) {
		super(notificationSender);
	}
	public void send(NotificationSenderDTO notificationSenderDTO){
		super.send(notificationSenderDTO);
		System.out.println(notificationSenderDTO.getPushContent());
	}
}
