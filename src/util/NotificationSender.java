package util;

import dto.NotificationSenderDTO;

public interface NotificationSender {
	void send(NotificationSenderDTO notificationSenderDTO);
}
