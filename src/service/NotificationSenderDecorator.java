package service;

import dto.NotificationSenderDTO;
import util.NotificationSender;

public class NotificationSenderDecorator implements NotificationSender {
	private NotificationSender notificationSender;

	public NotificationSenderDecorator(NotificationSender notificationSender) {
		this.notificationSender = notificationSender;
	}

	public NotificationSender getNotificationSender() {
		return notificationSender;
	}

	public void setNotificationSender(NotificationSender notificationSender) {
		this.notificationSender = notificationSender;
	}

	@Override
	public void send(NotificationSenderDTO notificationSenderDTO) {
		notificationSender.send(notificationSenderDTO);
	}
}
