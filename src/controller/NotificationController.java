package controller;

import decorator.EmailSender;
import decorator.PushSender;
import decorator.SmsSender;
import dto.NotificationSenderDTO;
import service.DefaultNotificationSender;
import service.NotificationSenderDecorator;
import util.NotificationSender;

public class NotificationController {

	public static void main(String[] args) {
		System.out.println("Only email");
		NotificationSender notificationSender1 = new DefaultNotificationSender();
		NotificationSenderDTO onlyEmailDTO = new NotificationSenderDTO();
		onlyEmailDTO.setSendEmail(true);
		sendNotifications(onlyEmailDTO);
		System.out.println("---------------------------------");

		System.out.println("Push and sms");
		NotificationSenderDTO pushAndSendDTO = new NotificationSenderDTO();
		pushAndSendDTO.setSendPush(true);
		pushAndSendDTO.setSendSms(true);
		sendNotifications(pushAndSendDTO);
		System.out.println("---------------------------------");

		System.out.println("All three");
		NotificationSenderDTO allThreeDTO = new NotificationSenderDTO();
		allThreeDTO.setSendEmail(true);
		allThreeDTO.setSendPush(true);
		allThreeDTO.setSendSms(true);
		sendNotifications(allThreeDTO);
		System.out.println("---------------------------------");

	}

	private static void sendNotifications(NotificationSenderDTO notificationSenderDTO){

		NotificationSender notificationSender = new DefaultNotificationSender();

		if (notificationSenderDTO.isSendEmail() && notificationSenderDTO.getEmailContent() != null){
			notificationSender = new EmailSender(notificationSender);
		}
		if (notificationSenderDTO.isSendSms() && notificationSenderDTO.getSmsContent() != null){
			notificationSender = new SmsSender(notificationSender);
		}
		if (notificationSenderDTO.isSendPush() && notificationSenderDTO.getPushContent() != null){
			notificationSender = new PushSender(notificationSender);
		}
		notificationSender.send(notificationSenderDTO);

	}
}
